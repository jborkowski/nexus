package com.jobo.actors.mocks

import java.time.ZonedDateTime

import akka.actor.{Actor, ActorLogging, ActorSystem}
import com.jobo.actors.ActorProtocol.{GetAccountingPeriodsBy, GetActiveAccountingPeriodsBy, GetContractorsBy, Msg}
import com.jobo.actors.{CurrencyConvert, CurrencyConverterHelper}
import com.jobo.db.Schema

import scala.concurrent.{ExecutionContextExecutor, Future}

object MockHelper {

  trait CurrencyConverterHelperMock extends CurrencyConverterHelper {
    val MockData: Map[String, Double] =
      Map("PLN" -> 4.2761, "CAD" -> 1.452, "HKD" -> 9.3506, "AUD" -> 1.4911, "SEK" -> 9.5058, "TRY" -> 4.114,
        "BRL" -> 3.7357, "KRW" -> 1353.03, "CZK" -> 26.081, "BGN" -> 1.9558, "GBP" -> 0.88043, "CHF" -> 1.148,
        "PHP" -> 61.305, "DKK" -> 7.4409, "JPY" -> 132.92, "USD" -> 1.1963, "EUR" -> 1.0)

    override def fetch: Future[Map[String, Double]] =
      Future(MockData)
  }

  class CurrencyConverterMockActor extends CurrencyConvert with CurrencyConverterHelperMock

  class StorageMock extends Actor with ActorLogging {

    import akka.pattern.pipe
    implicit val system: ActorSystem = context.system
    implicit def executor: ExecutionContextExecutor = system.dispatcher

    import com.jobo.protocol.ModelExtras._

    val mockAccountingPeriods: Seq[Schema.AccountingPeriod] = Seq(
      Schema.AccountingPeriod(
        Some(Schema.Model.ID(1)),
        Schema.Model.ContractorId("contractor-1"),
        ZonedDateTime.now(),
        None,
        Schema.Model.CategoryId(1001),
        Schema.Model.MoneyPln(100.0D),
        Schema.Model.MoneyPln(5.0D),
        Schema.Model.MoneyPln(5.0D)
      ),
      Schema.AccountingPeriod(
        Some(Schema.Model.ID(2)),
        Schema.Model.ContractorId("contractor-1"),
        ZonedDateTime.now(),
        Some(ZonedDateTime.now()),
        Schema.Model.CategoryId(1010),
        Schema.Model.MoneyPln(100.0D),
        Schema.Model.MoneyPln(5.0D),
        Schema.Model.MoneyPln(5.0D)
      )
    )

    def receive: Receive = {
      case m: Msg =>
        m match {
          case GetAccountingPeriodsBy(contractorId, activeFilter) =>
            Future (
              mockAccountingPeriods.filter(p => p.contractorId == contractorId.toDbModel)
            ).map(Right(_)) pipeTo sender()
          case GetActiveAccountingPeriodsBy(contractorId, categoryIds) =>
            Future (
              mockAccountingPeriods
                .filter(p => p.contractorId == contractorId.toDbModel)
                .filter(p => categoryIds.map(_.toDbModel).contains(p.categoryId))
            ).map(Right(_)) pipeTo sender()
          case GetContractorsBy(groupId) =>
            ???
        }
    }
  }
}
