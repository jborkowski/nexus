package com.jobo.actors

import java.util.Currency

import akka.actor.{ActorSystem, Props}
import akka.http.scaladsl.marshalling.ToResponseMarshallable
import akka.http.scaladsl.model.StatusCodes
import akka.testkit.{ImplicitSender, TestKit, TestProbe}
import com.jobo.actors.ActorProtocol._
import com.jobo.actors.mocks.MockHelper
import com.jobo.protocol.{Model, Protocol}
import org.scalatest.{BeforeAndAfterAll, FlatSpecLike, Matchers}

class ClientWalletSpec extends TestKit(ActorSystem("client-wallet"))
  with ImplicitSender with FlatSpecLike with Matchers with BeforeAndAfterAll {

  import MockHelper._

  override def afterAll {
    TestKit.shutdownActorSystem(system)
  }

  "Client Wallet" should "respond with wallet value" in {
    val contractorId: Model.ContractorId = Model.ContractorId("contractor-1")
    val cardNumber: Model.CardNumber = Model.CardNumber("card-number-for-contractor-1")

    val currencyConverter = system.actorOf(Props[CurrencyConverterMockActor])
    val storage = system.actorOf(Props[StorageMock])
    val client = system.actorOf(ClientWallet.props(contractorId, cardNumber, storage, currencyConverter))
    val requestHandler = TestProbe()

    Thread.sleep(1)

    import Protocol._
    import scala.concurrent.duration._

    client ! GetWallet(cardNumber, None, requestHandler.ref, false)
    val res = ToResponseMarshallable(StatusCodes.OK -> Protocol.ContractorWallet(0.0D, Currency.getInstance("PLN")))

    requestHandler.expectMsg(CompleteRequest(res))

      //(CompleteRequest(res))
   // expectMsgClass(classOf[CompleteRequest])
  }
}
