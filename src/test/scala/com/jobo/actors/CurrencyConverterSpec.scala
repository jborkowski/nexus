package com.jobo.actors

import java.util.Currency

import akka.actor.{ActorSystem, Props}
import akka.testkit.{ImplicitSender, TestKit}
import com.jobo.actors.ActorProtocol.{Exchange, ExchangeResponse}
import com.jobo.actors.mocks.MockHelper
import com.jobo.protocol.Model
import org.scalatest.{BeforeAndAfterAll, FlatSpecLike, Matchers}

class CurrencyConverterSpec extends TestKit(ActorSystem("currency-converter"))
  with ImplicitSender with FlatSpecLike with Matchers with BeforeAndAfterAll {

  import MockHelper._

  val CurrencyUSD = Currency.getInstance("USD")
  val CurrencyEUR = Currency.getInstance("EUR")
  val CurrencyPLN = Currency.getInstance("PLN")
  val CurrencyGBP = Currency.getInstance("GBP")
  val CurrencyRON = Currency.getInstance("RON")

  override def afterAll {
    TestKit.shutdownActorSystem(system)
  }

  "CurrencyConverter" should "respond for Exchange message" in {
    val actor = system.actorOf(Props[CurrencyConverterMockActor])
    Thread.sleep(1)
    actor ! Exchange(Model.Money(10), Currency.getInstance("USD"))
    expectMsgClass(classOf[ExchangeResponse])
  }

  "Currency Converter" should "convert 10 USD to EUR" in {
    val actor = system.actorOf(Props[CurrencyConverterMockActor])
    Thread.sleep(1)
    actor ! Exchange(Model.Money(10, CurrencyUSD), CurrencyEUR)
    expectMsg(ExchangeResponse(Right(Model.Money(8.359107247345984, CurrencyEUR))))
  }

  "Currency Converter" should "respond with error - Unknown Currency Code" in {
    val actor = system.actorOf(Props[CurrencyConverterMockActor])
    Thread.sleep(1)
    actor ! Exchange(Model.Money(10, CurrencyUSD), CurrencyRON)
    expectMsg(ExchangeResponse(Left("Unknown currency code")))
  }

  "Currency Converter" should "respond with error - Data not loaded" in {
    val actor = system.actorOf(Props[CurrencyConverterMockActor])

    actor ! Exchange(Model.Money(10, CurrencyUSD), CurrencyRON)
    expectMsg(ExchangeResponse(Left("Empty currency rate table, try again later.")))
  }
}
