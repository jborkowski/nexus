package com.jobo.utils

import java.sql.Timestamp
import java.time.{LocalDateTime, ZoneId, ZonedDateTime}

import com.jobo.db.Schema.Model
import com.jobo.protocol.Protocol._
import slick.jdbc.PostgresProfile

trait CustomPostgresProfile extends PostgresProfile with ConfigSupport {
  import api._

  implicit def localDateTimeToTimestamp = MappedColumnType.base[LocalDateTime, Timestamp] (
    l => Timestamp.valueOf(l),
    d => d.toLocalDateTime
  )

  implicit val zonedDateTimeColumnType = MappedColumnType.base[ZonedDateTime, Timestamp](
    dateTime => Timestamp.from(dateTime.toInstant),
    timestamp => ZonedDateTime.ofInstant(timestamp.toInstant, ZoneId.systemDefault())
  )

  implicit def emailToString = MappedColumnType.base[Model.Email, String] (
    e => e.email,
    s => Model.Email(s)
  )

  implicit def nickToString = MappedColumnType.base[Model.Nick, String] (
    n => n.value,
    s => Model.Nick(s)
  )

  implicit def idToInt = MappedColumnType.base[Model.ID, Int] (
    i => i.id,
    s => Model.ID(s)
  )

  //TODO Add more
}

object CustomPostgresProfile extends CustomPostgresProfile
