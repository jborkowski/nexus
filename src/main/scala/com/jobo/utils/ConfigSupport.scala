package com.jobo.utils

import com.typesafe.config.ConfigFactory

trait ConfigSupport {
  lazy val config = ConfigFactory.load()
}
