package com.jobo.utils

import org.flywaydb.core.Flyway

trait FlywaySupport extends ConfigSupport {
  private lazy val dbConfig = config.getConfig("db")

  private lazy val dbUrl = dbConfig.getString("url")
  private lazy val dbUser = dbConfig.getString("user")
  private lazy val dbPass = dbConfig.getString("password")

  private[this] val flyway = new Flyway()
  flyway.setDataSource(dbUrl, dbUser, dbPass)

  def migrateDatabaseSchema() : Unit = flyway.migrate()

  def dropDatabase() : Unit = flyway.clean()
}
