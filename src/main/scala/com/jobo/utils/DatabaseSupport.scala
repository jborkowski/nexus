package com.jobo.utils

import com.zaxxer.hikari.{HikariConfig, HikariDataSource}

trait DatabaseSupport extends ConfigSupport {
  private lazy val dbConfig = config.getConfig("db")

  private lazy val dbUrl = dbConfig.getString("url")
  private lazy val dbUser = dbConfig.getString("user")
  private lazy val dbPass = dbConfig.getString("password")

  val driver = CustomPostgresProfile

  import driver.api._

  implicit lazy val db = connect(dbUrl, dbUser, dbPass)

  def connect(url: String, user: String, pass: String): Database  = {
    val hikariConfig = new HikariConfig()

    hikariConfig.setMaximumPoolSize(10)
    hikariConfig.setJdbcUrl(url)
    hikariConfig.setUsername(user)
    hikariConfig.setPassword(pass)

    val dataSource = new HikariDataSource(hikariConfig)

    Database.forDataSource(dataSource, maxConnections = None)
  }

  def closeDbConnection(): Unit = {
    db.close()
  }

  def createSession(): Unit = {
    db.createSession()
  }
}