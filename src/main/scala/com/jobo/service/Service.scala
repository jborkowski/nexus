package com.jobo.service

import java.util.Currency

import akka.actor.{ActorRef, ActorSystem, Props}
import akka.event.LoggingAdapter
import akka.http.scaladsl.marshalling.ToResponseMarshallable
import akka.http.scaladsl.model.{ContentTypes, HttpEntity, HttpResponse}
import akka.http.scaladsl.server.{Directives, ExceptionHandler, RouteResult}
import akka.stream.Materializer
import com.jobo.protocol.{Model, Protocol}
import akka.http.scaladsl.model.StatusCodes._
import akka.http.scaladsl.server.directives.Credentials
import akka.pattern.ask
import akka.util.Timeout
import com.jobo.actors.ActorProtocol._
import com.jobo.actors.RequestHandler

import scala.concurrent.ExecutionContextExecutor
import scala.concurrent.duration._

trait Service extends Directives {
  import Protocol._
  import com.jobo.protocol.ProtocolExtras._
  import com.jobo.utils.ImperativeRequestContext._

  implicit val system: ActorSystem
  implicit def executor: ExecutionContextExecutor
  implicit val materializer: Materializer

  def supervisorActor: ActorRef

  val logger: LoggingAdapter

  val customExceptionHandler = ExceptionHandler {
    case e: IllegalArgumentException =>
      complete(HttpResponse(InternalServerError, entity = s"Please provide correct params: ${e.getMessage}"))

    case t: Throwable =>
      extractUri { uri =>
        println(s"Request to $uri could not be handled normally")
        complete(HttpResponse(InternalServerError, entity = s"Something went wrong: ${t.getMessage}"))
      }
  }

  val routes =
    logRequest("loyality") {
      handleExceptions(customExceptionHandler) {
        pathPrefix("api") {
          transactionRoutes ~ walletInfoRoutes
        }
      }
    }

  private val transactionRoutes =
    pathPrefix("transactions") {
      (post & entity(as[Protocol.Transaction])) { t =>
        imperativelyComplete { implicit ctx =>
          system.actorOf(RequestHandler.props(supervisorActor)) ! ProcessTransactions(t.toModel)
        }
      }
    }

  implicit val askTimeout: Timeout = 3 seconds

  private val walletInfoRoutes =
    pathPrefix("card-info") {
      (get &
        authenticateBasic(realm = "secure site", checkAuth) &
        path(Segment) & parameters('currency.as[String].?)) { (_, cardNumber, currencyOpt) =>
          imperativelyComplete { implicit ctx =>
            system.actorOf(RequestHandler.props(supervisorActor)) !
              GetWalletState(Model.CardNumber(cardNumber), currencyOpt.map(Currency.getInstance))
          }
        }
      } ~
      pathPrefix("accounting-period") {
        (get &
          authenticateBasic(realm = "secure site", checkAuth) &
          path(Segment) & parameters('active.as[Boolean].?)) { (_, contractorId, activeFilterOpt) =>
            imperativelyComplete { implicit ctx =>
              system.actorOf(RequestHandler.props(supervisorActor)) !
                GetAccountingPeriods(Model.ContractorId(contractorId), activeFilterOpt)
            }
          }
      } ~
      pathPrefix("group-wallet") {
        (get &
          authenticateBasic(realm = "secure site", checkAuth) & path(Segment) &
          parameters('currency.as[String].?)) { (_, groupId, currencyOpt) =>
          imperativelyComplete { implicit ctx =>
            system.actorOf(RequestHandler.props(supervisorActor)) !
              GetGroupWalletState(Model.GroupId(groupId), currencyOpt.map(Currency.getInstance))
          }
        }
      }

  def checkAuth(credentials: Credentials): Option[String] =
    credentials match {
      case p @ Credentials.Provided(id) if p.verify("p4ssw0rd") => Some(id)
      case _ => None
    }
}
