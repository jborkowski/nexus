package com.jobo.protocol

import java.time.format.DateTimeFormatter
import java.time.{LocalDateTime, ZonedDateTime}
import java.util.Currency

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import pl.iterators.kebs.json.KebsSpray
import spray.json._

object Protocol extends JsonProtocol with KebsSpray {
  case class ContractorId(id: String) {
    require(ContractorId.isValid(id), s"'$id' must be valid contractor Id")
  }

  object ContractorId {
    def isValid(id: String): Boolean = !id.isEmpty
  }

  case class PosId(id: String) {
    require(PosId.isValid(id), s"'$id' must be valid pos Id")
  }

  object PosId {
    def isValid(id: String): Boolean = !id.isEmpty
  }

  case class CardNumber(number: String)

  case class ProductNumber(number: String)

  case class TransactionId(id: String)

  case class Quantity(number: Int)

  case class TransactionDatetime(date: ZonedDateTime)

  case class Email(email: String) {
    require(Email.isValid(email), s"'$email' must be a valid email address")
  }

  object Email {
    def isValid(email: String): Boolean =
      """^\S+@\S+$""".r.unapplySeq(email).isDefined
  }

  case class Password(pass: String) {
    require(Password.isValid(pass), s"'$pass' must be a valid password")
  }

  object Password {
    def isValid(pass: String): Boolean = !pass.isEmpty
  }

  case class CategoryId(id: Int) {
    require(CategoryId.isValid(id), s"'$id' must be valid category ID")
  }

  object CategoryId {
    def isValid(id: Int): Boolean = id >= 0
  }

  case class MoneyPln(money: Double) {
    require(MoneyPln.isValid(money), s"'$money' must be not negative")
  }

  object MoneyPln {
    def isValid(money: Double): Boolean = money >= 0.0D
  }

  case class Product(
    product_number: ProductNumber,
    category_id: CategoryId,
    quantity: Quantity,
    gross_cost: MoneyPln
  )

  case class Transaction(
    contractor_id: ContractorId,
    pos_id: PosId,
    card_number: CardNumber,
    transaction_id: TransactionId,
    transaction_datetime: TransactionDatetime,
    total_gross_cost: MoneyPln,
    products: Seq[Product]
  )

  case class ErrorMessage(errorMessage: String)

  case class CategoryName(name: String)

  case class Category(category_id: CategoryId, category_name: CategoryName)

  case class ContractorWallet(value: Double, currency: Currency)

  case class Contractor(contractor_id: ContractorId, wallet: ContractorWallet)

  case class ContractorAccountingPeriodResponse(accounting_period: Seq[AccountingPeriodResponse])

  case class AccountingPeriodResponse(
    category: Category,
    contractor_id: ContractorId,
    period_start: ZonedDateTime,
    period_end: Option[ZonedDateTime],
    price_threshold: MoneyPln,
    client_salary: MoneyPln,
    system_salary: MoneyPln
  )

  case class GroupId(id: String)

  case class ContractorGroup(
    group_id: GroupId,
    sum: Option[ContractorWallet],
    contractors: Seq[Contractor]
  )
}

object ProtocolExtras {
  implicit class ProductNumberExtensions(p: Protocol.ProductNumber) {
    def toModel: Model.ProductNumber =
      Model.ProductNumber(p.number)
  }

  implicit class CategoryIdExtensions(c: Protocol.CategoryId) {
    def toModel: Model.CategoryId =
      Model.CategoryId(c.id)
  }

  implicit class CategoryNameExtensions(c: Protocol.CategoryName) {
    def toModel: Model.CategoryName =
      Model.CategoryName(c.name)
  }

  implicit class CategoryExtensions(c: Protocol.Category) {
    def toModel: Model.Category =
      Model.Category(c.category_id.toModel, c.category_name.toModel)
  }

  implicit class ContractorIdExtensions(c: Protocol.ContractorId) {
    def toModel: Model.ContractorId =
      Model.ContractorId(c.id)
  }

  implicit class QuantityExtensions(q: Protocol.Quantity) {
    def toModel: Model.Quantity =
      Model.Quantity(q.number)
  }

  implicit class CardNumberExtensions(q: Protocol.CardNumber) {
    def toModel: Model.CardNumber =
      Model.CardNumber(q.number)
  }

  implicit class TransactionIdExtensions(t: Protocol.TransactionId) {
    def toModel: Model.TransactionId =
      Model.TransactionId(t.id)
  }

  implicit class TransactionDatetimeExtensions(t: Protocol.TransactionDatetime) {
    def toModel: Model.TransactionDatetime =
      Model.TransactionDatetime(t.date)
  }

  implicit class PosIdExtensions(p: Protocol.PosId) {
    def toModel: Model.PosId =
      Model.PosId(p.id)
  }

  implicit class MoneyPlnExtensions(m: Protocol.MoneyPln) {
    def toModel: Model.Money =
      Model.Money(m.money)
  }

  implicit class ProductExtensions(p: Protocol.Product) {
    def toModel: Model.Product = {
      Model.Product(
        productNumber = p.product_number.toModel,
        categoryId = p.category_id.toModel,
        quantity = p.quantity.toModel,
        grossCost = p.gross_cost.toModel
      )
    }
  }

  implicit class TransactionExtensions(e: Protocol.Transaction) {
    def toModel: Model.Transaction =
      Model.Transaction(
        contractorId = e.contractor_id.toModel,
        posId = e.pos_id.toModel,
        cardNumber = e.card_number.toModel,
        transactionId = e.transaction_id.toModel,
        transactionDatetime = e.transaction_datetime.toModel,
        totalGrossCost = e.total_gross_cost.toModel,
        products = e.products.map(_.toModel)
      )
  }
}

trait JsonProtocol extends DefaultJsonProtocol with SprayJsonSupport {
  import Protocol._
  val dateTimeFormatter = DateTimeFormatter.ISO_OFFSET_DATE_TIME

  implicit val localDateTimeFormat = new JsonFormat[LocalDateTime] {
    def read(json: JsValue): LocalDateTime = json match {
      case JsString(value) => LocalDateTime.parse(value)
      case _ => throw DeserializationException("LocalDateTime expected")
    }

    def write(obj: LocalDateTime): JsValue = JsString(obj.toString)
  }

  implicit val dateJsonFormat = new JsonFormat[ZonedDateTime] {

    override def write(obj: ZonedDateTime) = JsString(dateTimeFormatter.format(obj))

    override def read(json: JsValue) = json match {
      case JsString(s) => ZonedDateTime.parse(s, dateTimeFormatter)
      case other => throw DeserializationException("Cannot parse json value " + other + " as timestamp")
    }
  }

  implicit val emailJsonFormat = new JsonFormat[Email] {
    def read(json: JsValue): Email = json match {
      case JsString(value) => Email(value)
      case _ => throw DeserializationException("Email expected")
    }

    def write(email: Email): JsValue = JsString(email.email)
  }

  implicit val currencyFormat = new JsonFormat[Currency] {
    def read(json: JsValue): Currency = json match {
      case JsString(v) => Currency.getInstance(v)
      case _ => throw DeserializationException("Currency expected")
    }

    def write(obj: Currency): JsValue =
      JsString(obj.getCurrencyCode)
  }

  implicit val categoryNameJsonFormat = new JsonFormat[CategoryName] {
    def read(json: JsValue): CategoryName = json match {
      case JsString(value) => CategoryName(value)
      case _ => throw DeserializationException("CategoryName expected")
    }

    def write(category: CategoryName): JsValue = JsString(category.name)
  }

  implicit val passwordJsonFormat = new JsonFormat[Password] {
    def read(json: JsValue): Password = json match {
      case JsString(value) => Password(value)
      case _ => throw DeserializationException("Password expected")
    }

    def write(pass: Password): JsValue = JsString(pass.pass)
  }

  // todo: deserializationError instead DeserializationException and Try
  implicit val moneyJsonFormat = new JsonFormat[MoneyPln] {
    def read(json: JsValue): MoneyPln = json match {
      case JsNumber(value) => MoneyPln(value.toDouble)
      case _ => throw DeserializationException(s"MoneyPln expected: $json")
    }

    def write(money: MoneyPln): JsValue = JsString(money.money.toString)
  }

  implicit val categoryIdFormat = new JsonFormat[CategoryId] {
    def read(json: JsValue): CategoryId = json match {
      case JsNumber(value) => CategoryId(value.toIntExact)
      case _ => throw DeserializationException("Category ID expected")
    }

    def write(categoryId: CategoryId): JsValue = JsNumber(categoryId.id)
  }

  implicit val contractorIdFormat = new JsonFormat[ContractorId] {
    def read(json: JsValue): ContractorId = json match {
      case JsString(value) => ContractorId(value)
      case _ => throw DeserializationException("Contractor ID expected")
    }

    def write(contractorId: ContractorId): JsValue = JsString(contractorId.id)
  }

  implicit val groupIdFormat = new JsonFormat[GroupId] {
    def read(json: JsValue): GroupId = json match {
      case JsString(value) => GroupId(value)
      case _ => throw DeserializationException("Group ID expected")
    }

    def write(id: GroupId): JsValue = JsString(id.id)
  }


  implicit val posIdFormat = new JsonFormat[PosId] {
    def read(json: JsValue): PosId = json match {
      case JsString(value) => PosId(value)
      case _ => throw DeserializationException("Position ID expected")
    }

    def write(posId: PosId): JsValue = JsString(posId.id)
  }

  implicit val cardNumberFormat = new JsonFormat[CardNumber] {
    def read(json: JsValue): CardNumber = json match {
      case JsString(value) => CardNumber(value)
      case _ => throw DeserializationException("Card Number expected")
    }

    def write(cardNumber: CardNumber): JsValue = JsString(cardNumber.number)
  }

  implicit val productNumberFormat = new JsonFormat[ProductNumber] {
    def read(json: JsValue): ProductNumber = json match {
      case JsString(value) => ProductNumber(value)
      case _ => throw DeserializationException("Product Number expected")
    }

    def write(productNumber: ProductNumber): JsValue = JsString(productNumber.number)
  }

  implicit val transactionIdFormat = new JsonFormat[TransactionId] {
    def read(json: JsValue): TransactionId = json match {
      case JsString(value) => TransactionId(value)
      case _ => throw DeserializationException("Transaction ID expected")
    }

    def write(transactionId: TransactionId): JsValue = JsString(transactionId.id)
  }

  implicit val quantityFormat = new JsonFormat[Quantity] {
    def read(json: JsValue): Quantity = json match {
      case JsNumber(value) => Quantity(value.toIntExact)
      case _ => throw DeserializationException("Quantity expected")
    }

    def write(quantity: Quantity): JsValue = JsNumber(quantity.number)
  }

  implicit val transactionDateTimeFormat = new JsonFormat[TransactionDatetime] {
    def read(json: JsValue): TransactionDatetime = json match {
      case JsString(value) => TransactionDatetime(ZonedDateTime.parse(value, dateTimeFormatter))
      case _ => throw DeserializationException("Transaction Date Time expected")
    }

    def write(transactionDatetime: TransactionDatetime): JsValue =
      JsString(dateTimeFormatter.format(transactionDatetime.date))
  }
}
