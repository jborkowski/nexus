package com.jobo.protocol

import java.time.ZonedDateTime
import java.util.Currency

import com.jobo.db.Schema

object Model {
  final case class GroupId(id: String)

  final case class ContractorId(id: String)

  final case class PosId(id: String)

  final case class CardNumber(number: String)

  final case class ProductNumber(number: String)

  final case class TransactionId(id: String)

  final case class Quantity(number: Int)

  final case class TransactionDatetime(date: ZonedDateTime)

  final case class Email(email: String)

  final case class Password(pass: String)

  final case class CategoryId(id: Int)

  final case class CategoryName(name: String)

  final case class Category(categoryId: CategoryId, categoryName: CategoryName)

  final case class Money(money: Double, currency: Currency = Currency.getInstance("PLN"))

  final case class ContractorGroup(groupId: Model.GroupId, contractorInfo: Seq[(Model.ContractorId, Model.CardNumber)])

  final case class Product(
    productNumber: Model.ProductNumber,
    categoryId: Model.CategoryId,
    quantity: Model.Quantity,
    grossCost: Model.Money
  )

  final case class Transaction(
    contractorId: Model.ContractorId,
    posId: Model.PosId,
    cardNumber: Model.CardNumber,
    transactionId: Model.TransactionId,
    transactionDatetime: Model.TransactionDatetime,
    totalGrossCost: Model.Money,
    products: Seq[Model.Product]
  )

  final case class AccountingPeriod(
    contractorId: Model.ContractorId,
    periodStart: ZonedDateTime,
    periodEnd: Option[ZonedDateTime],
    categoryId: Model.CategoryId,
    priceThreshold: Model.Money,
    clientSalary: Model.Money,
    systemSalary: Model.Money
  )

  case class Contractor(
    contractorId: ContractorId,
    wallet: ContractorWallet
  )

  case class ContractorWallet(value: Double, currency: Currency)
}

object ModelExtras {

  implicit class MoneyExtension(m: Model.Money) {
    def toProto: Protocol.MoneyPln =
      Protocol.MoneyPln(m.money)
  }

  implicit class GroupIdExtension(g: Model.GroupId) {
    def toDbModel: Schema.Model.GroupId =
      Schema.Model.GroupId(g.id)
    def toProto: Protocol.GroupId =
      Protocol.GroupId(g.id)
  }

  implicit class ContractorIdExtensions(c: Model.ContractorId) {
    def toDbModel: Schema.Model.ContractorId =
      Schema.Model.ContractorId(c.id)
    def toProto: Protocol.ContractorId =
      Protocol.ContractorId(c.id)
  }

  implicit class CategoryIdExtensions(c: Model.CategoryId) {
    def toDbModel: Schema.Model.CategoryId =
      Schema.Model.CategoryId(c.id)

    def toProto: Protocol.CategoryId =
      Protocol.CategoryId(c.id)
  }

  implicit class CategoryNameExtension(n: Model.CategoryName) {
    def toProto: Protocol.CategoryName =
      Protocol.CategoryName(n.name)
  }

  implicit class CategoryExtensions(c: Model.Category) {
    def toProto: Protocol.Category =
      Protocol.Category(c.categoryId.toProto, c.categoryName.toProto)
  }

  implicit class ContractorWalletExtension(cw: Model.ContractorWallet) {
    def toProto: Protocol.ContractorWallet =
      Protocol.ContractorWallet(cw.value, cw.currency)
  }

  implicit class ContractorExtension(c: Model.Contractor) {
    def toProto: Protocol.Contractor =
      Protocol.Contractor(c.contractorId.toProto, c.wallet.toProto)
  }

  implicit class AccountingPeriodExtension(a: Model.AccountingPeriod) {
    def toProto(desc: Model.CategoryId => Model.Category): Protocol.AccountingPeriodResponse = {
      Protocol.AccountingPeriodResponse(
        category = desc(a.categoryId).toProto,
        contractor_id = a.contractorId.toProto,
        period_start = a.periodStart,
        period_end = a.periodEnd,
        price_threshold = a.priceThreshold.toProto,
        client_salary = a.clientSalary.toProto,
        system_salary = a.systemSalary.toProto
      )
    }
  }

}
