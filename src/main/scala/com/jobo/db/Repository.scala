package com.jobo.db

import java.time.ZonedDateTime

import cats.data.EitherT
import com.jobo.protocol.Protocol
import com.jobo.utils.ConfigSupport
import com.jobo.utils.CustomPostgresProfile._
import slick.lifted.ProvenShape

import scala.concurrent.{ExecutionContextExecutor, Future}
import scala.language.higherKinds

trait Repository extends ConfigSupport {
  import api._

  val db: Database
  implicit def executor: ExecutionContextExecutor

  type Error = String
  type FutureEither[T] = EitherT[Future, Error, T]

  import slick.lifted.CanBeQueryCondition

  case class MaybeFilter[X, Y, C[_]](query: slick.lifted.Query[X, Y, C]) {
    def filter[T,R:CanBeQueryCondition](data: Option[T])(f: T => X => R) = {
      data.map(v => MaybeFilter(query.withFilter(f(v)))).getOrElse(this)
    }
  }
}

trait AccountingPeriodRepository extends Repository {
  import pl.iterators.kebs._
  import api._
  import Schema.{Model, accountingPeriods, AccountingPeriod}

  def getActiveAccountingPeriodBy(contractorId: Model.ContractorId,
                                  categoryIds: List[Model.CategoryId]): FutureEither[Seq[AccountingPeriod]]= {
    val now = ZonedDateTime.now()

    val findAction = for {
      a <- accountingPeriods
      if a.contractorId === contractorId
      if a.categoryId inSetBind categoryIds
      if a.periodStart <= now
      if a.periodEnd >= now || a.periodEnd.isEmpty
    } yield a

    //findAction.statements.foreach(println)

    EitherT(
      db
        .run(findAction.result)
        .map(Right(_))
        .recover {
          case t =>
            Left(t.getMessage)
        }
    )
  }

  def getAccountingPeriodBy(contractorId: Model.ContractorId, activeFilter: Option[Boolean]): FutureEither[Seq[AccountingPeriod]] = {

    val now = ZonedDateTime.now()

    val findAction =
      accountingPeriods
        .filter(_.contractorId === contractorId)

    val (aF, iF) = activeFilter match {
      case None             => (None, None)
      case a @ Some(true)   => (a, None)
      case a @ Some(false)  => (None, a)
    }

    val f = MaybeFilter(findAction)
      .filter(aF)(_ => e => e.periodStart <= now  && (e.periodEnd >= now || e.periodEnd.isEmpty))
      .filter(iF)(_ => e => e.periodStart >= now  ||(e.periodEnd <= now && !e.periodEnd.isEmpty))
      .query

    EitherT(
      db
        .run(f.result)
        .map(Right(_))
        .recover {
          case t =>
            Left(t.getMessage)
        }
    )
  }


}

trait UserAccountRepository extends Repository {

}

trait ContractorGroupRepository extends Repository {
  import pl.iterators.kebs._
  import api._
  import Schema.{contractorGroups, Model, ContractorGroup}

  def getContractorByGroup(groupId: Model.GroupId): FutureEither[Seq[ContractorGroup]] = {
    val findAction = contractorGroups.filter(_.groupId === groupId)

    EitherT(
      db
        .run(findAction.result)
        .map(Right(_))
        .recover {
          case t =>
            Left(t.getMessage)
        }
    )
  }

}

object Schema {
  import api._
  import pl.iterators.kebs._

  case class AccountingPeriod(
    id: Option[Model.ID] = None,
    contractorId: Model.ContractorId,
    periodStart: ZonedDateTime,
    periodEnd: Option[ZonedDateTime],
    categoryId: Model.CategoryId,
    priceThreshold: Model.MoneyPln,
    clientSalary: Model.MoneyPln,
    systemSalary: Model.MoneyPln
  )

  case class UserAccount(
    id: Option[Model.ID] = None,
    email: Model.Email,
    nick: Model.Nick,
    password: Model.Password,
    createdAt: ZonedDateTime
  )

  case class ContractorGroup(
    id: Option[Model.ID] = None,
    contractorId: Model.ContractorId,
    cardNumber: Model.CardNumber,
    groupId: Model.GroupId,
    createdAt: ZonedDateTime
  )

  class AccountingPeriods(tag: Tag) extends Table[AccountingPeriod](tag, "accounting_period") {
    def id              = column[Model.ID]("id", O.PrimaryKey, O.AutoInc)
    def contractorId    = column[Model.ContractorId]("contractor_id")
    def periodStart     = column[ZonedDateTime]("period_start")
    def periodEnd       = column[Option[ZonedDateTime]]("period_end")
    def categoryId      = column[Model.CategoryId]("category_id")
    def priceThreshold  = column[Model.MoneyPln]("price_threshold")
    def clientSalary    = column[Model.MoneyPln]("client_salary")
    def systemSalary    = column[Model.MoneyPln]("system_salary")

    def * : ProvenShape[AccountingPeriod] =
      (id.?, contractorId, periodStart, periodEnd, categoryId, priceThreshold, clientSalary, systemSalary) <> (AccountingPeriod.tupled, AccountingPeriod.unapply)
  }

  class UserAccounts(tag: Tag) extends Table[UserAccount](tag, "user_account") {
    def id        = column[Model.ID]("id", O.PrimaryKey, O.AutoInc)
    def email     = column[Model.Email]("email")
    def nick      = column[Model.Nick]("nick")
    def password  = column[Model.Password]("password")
    def createdAt = column[ZonedDateTime]("created_at")

    def * : ProvenShape[UserAccount] =
      (id.?, email, nick, password, createdAt) <> (UserAccount.tupled, UserAccount.unapply)
  }

  class ContractorGroups(tag: Tag) extends Table[ContractorGroup](tag, "contractor_group") {
    def id            = column[Model.ID]("id", O.PrimaryKey, O.AutoInc)
    def contractorId  = column[Model.ContractorId]("contractor_id")
    def cardNumber    = column[Model.CardNumber]("card_number")
    def groupId       = column[Model.GroupId]("group_id")
    def createdAt     = column[ZonedDateTime]("created_at")

    def * : ProvenShape[ContractorGroup] =
      (id.?, contractorId, cardNumber, groupId, createdAt) <> (ContractorGroup.tupled, ContractorGroup.unapply)
  }

  val accountingPeriods = TableQuery[AccountingPeriods]
  val userAccounts = TableQuery[UserAccounts]
  val contractorGroups = TableQuery[ContractorGroups]

  val schema = accountingPeriods.schema ++ userAccounts.schema ++ contractorGroups.schema

  def createSchemaAction = schema.create
  def dropSchemaAction = schema.drop

  object Model {
    case class ID(id: Int)                  extends AnyVal
    case class Email(email: String)         extends AnyVal
    case class Nick(value: String)          extends AnyVal
    case class Password(value: String)      extends AnyVal
    case class ContractorId(id: String)     extends AnyVal
    case class GroupId(id: String)          extends AnyVal
    case class CategoryId(value: Int)       extends AnyVal
    case class CardNumber(number: String)   extends AnyVal
    case class MoneyPln(value: Double)      extends AnyVal
  }

  object DbModelExtras {
    implicit class CategoryIdExtension(c: Model.CategoryId) {
      def toModel: com.jobo.protocol.Model.CategoryId =
        com.jobo.protocol.Model.CategoryId(c.value)

      def toProto: Protocol.CategoryId =
        Protocol.CategoryId(c.value)
    }

    implicit class MoneyPlnExtension(m: Model.MoneyPln) {
      def toProto: Protocol.MoneyPln =
        Protocol.MoneyPln(m.value)

      def toModel: com.jobo.protocol.Model.Money =
        com.jobo.protocol.Model.Money(m.value)
    }

    implicit class ContractorIdExtension(c: Model.ContractorId) {
      def toModel: com.jobo.protocol.Model.ContractorId =
        com.jobo.protocol.Model.ContractorId(c.id)
    }

    implicit class CardNumberExtension(c: Model.CardNumber) {
      def toModel: com.jobo.protocol.Model.CardNumber =
        com.jobo.protocol.Model.CardNumber(c.number)
    }

    implicit class GroupIdExtensions(g: Model.GroupId) {
      def toModel: com.jobo.protocol.Model.GroupId =
        com.jobo.protocol.Model.GroupId(g.id)
    }

    implicit class ContractorGroupExtensions(c: Seq[Schema.ContractorGroup]) {
      def toModel: com.jobo.protocol.Model.ContractorGroup =
        com.jobo.protocol.Model.ContractorGroup(
          c.map(_.groupId).head.toModel, c.map(c => (c.contractorId.toModel, c.cardNumber.toModel))
        )
    }

    implicit class AccountingPeriodsExtension(a: Schema.AccountingPeriod) {
      def toModel: com.jobo.protocol.Model.AccountingPeriod =
        com.jobo.protocol.Model.AccountingPeriod(
          contractorId = a.contractorId.toModel,
          periodStart = a.periodStart,
          periodEnd = a.periodEnd,
          categoryId = a.categoryId.toModel,
          priceThreshold = a.priceThreshold.toModel,
          clientSalary = a.clientSalary.toModel,
          systemSalary = a.systemSalary.toModel
        )
    }
  }
}