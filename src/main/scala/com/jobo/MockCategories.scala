package com.jobo

import akka.actor.ActorSystem
import akka.event.Logging
import akka.http.scaladsl.Http
import akka.http.scaladsl.server.Directives
import akka.stream.ActorMaterializer
import com.jobo.protocol.Protocol

object MockCategories extends App with Directives {

  import Protocol._

  implicit val system = ActorSystem()
  implicit val materializer = ActorMaterializer()
  implicit val executor = system.dispatcher

  val Interface = "127.0.0.1"
  val Port = 9000
  val catSeq = Seq(
    Category(CategoryId(1), CategoryName("Spożywka")),
    Category(CategoryId(2), CategoryName("RTV")),
    Category(CategoryId(9), CategoryName("Bielizna")),
    Category(CategoryId(10), CategoryName("Internet")),
    Category(CategoryId(3), CategoryName("Elektronika"))
  )

  val logger = Logging(system, getClass)

  val routes = logRequest("category-mock") {
    pathPrefix("api") {
      pathPrefix("categories") {
        get {
          complete(
            catSeq
          )
        }
      }
    }
  }

  Http().bindAndHandle(routes, Interface, Port)

}
