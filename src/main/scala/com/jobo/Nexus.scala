package com.jobo

import akka.actor.{ActorSystem, Props}
import akka.event.Logging
import akka.http.scaladsl.Http
import akka.stream.{ActorMaterializer, Supervision}
import com.jobo.actors.{CategoryDescriptionProvider, CurrencyConvert, Storage, Supervisor}
import com.jobo.db.AccountingPeriodRepository
import com.jobo.service.Service
import com.jobo.utils.{ConfigSupport, DatabaseSupport, FlywaySupport}

import scala.concurrent.Future

object Nexus extends App
             with FlywaySupport
             with DatabaseSupport
             with Service
             with ConfigSupport {

  override implicit val system = ActorSystem()
  override implicit val materializer = ActorMaterializer()
  override implicit val executor = system.dispatcher

  override val logger = Logging(system, getClass)

  dropDatabase()
  migrateDatabaseSchema()

  val currencyConvertActor = system.actorOf(CurrencyConvert.props)
  val storageActor = system.actorOf(Props[Storage])
  val categoryDescActor = system.actorOf(Props[CategoryDescriptionProvider])

  override val supervisorActor = system.actorOf(Supervisor.prop(storageActor, currencyConvertActor, categoryDescActor))

  db.createSession()

  Http().bindAndHandle(routes, config.getString("http.interface"), config.getInt("http.port"))
}
