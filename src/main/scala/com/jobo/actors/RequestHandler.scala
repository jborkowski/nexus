package com.jobo.actors

import akka.actor.{Actor, ActorLogging, ActorRef, Props}
import com.jobo.actors.ActorProtocol._
import com.jobo.utils.ImperativeRequestContext

object RequestHandler {
  def props(supervisor: ActorRef)(implicit ctx: ImperativeRequestContext) =
    Props(new RequestHandler(ctx, supervisor))
}

class RequestHandler(ctx: ImperativeRequestContext, supervisor: ActorRef) extends Actor with ActorLogging {
  def receive = {
    case h: HandleRequest =>
      h match {
        case ProcessTransactions(t) =>
          supervisor ! ConsumeTransaction(t.contractorId, t.cardNumber, t.products, self)
        case GetWalletState(c, t) =>
          supervisor ! GetWallet(c, t, self)
        case GetAccountingPeriods(contractorId, activeFilter) =>
          supervisor ! AccountingPeriods(contractorId, activeFilter, self)
        case GetGroupWalletState(g, targetCurrency) =>
          supervisor ! GetGroupWallet(g, targetCurrency, self)
        case CompleteRequest(obj) =>
          ctx.complete(obj)
          context.stop(self)
        case CompleteFailureRequest(t) =>
          ctx.fail(t)
          context.stop(self)
      }
  }
}