package com.jobo.actors

import java.util.Currency

import akka.actor.{Actor, ActorLogging, ActorRef, ActorSystem, Props}
import akka.http.scaladsl.Http
import akka.http.scaladsl.client.RequestBuilding
import akka.http.scaladsl.model.{HttpResponse, StatusCodes, Uri}
import com.jobo.actors.CurrencyConvert._
import akka.http.scaladsl.marshallers.xml.ScalaXmlSupport._
import akka.http.scaladsl.unmarshalling.Unmarshal

import scala.concurrent.{ExecutionContextExecutor, Future}
import scala.concurrent.duration._
import scala.concurrent.duration.{Duration, FiniteDuration}
import akka.pattern.pipe
import akka.stream.{ActorMaterializer, Materializer}
import com.jobo.actors.ActorProtocol._
import com.jobo.protocol.Model

import scala.xml.NodeSeq

object CurrencyConvert {
  sealed trait Internal
  case object RefreshRate extends Internal
  case class FetchComplete(rates: Map[String, Double]) extends Internal

  def props = Props[CurrencyConvert]
}

class CurrencyConvert extends Actor with ActorLogging with CurrencyConverterHelper {

  implicit val system: ActorSystem = context.system
  implicit val executor: ExecutionContextExecutor = system.dispatcher
  implicit val materializer: Materializer = ActorMaterializer()

  private val refreshDuration: FiniteDuration =
    Duration(context.system.settings.config.getDuration("cantor.refresh-exchange-rate-duration", MINUTES), MINUTES)

  val currencyRateUri = Uri(context.system.settings.config.getString("cantor.currency-convert-url"))

  override def preStart(): Unit =
    self ! RefreshRate

  def receive: Receive = state()

  private def state(rates: Map[String, Double] = Map.empty): Receive = {
    case t: Internal =>
      t match {
        case RefreshRate =>
          fetch map FetchComplete pipeTo self
        case FetchComplete(r) =>
          context.become(state(r))
          context.system.scheduler.scheduleOnce(refreshDuration, self, RefreshRate)
      }
    case m: Msg =>
      m match {
        case Exchange(w, t) =>
          sender() ! ExchangeResponse(covert(w.money, w.currency, t, rates))
      }
    case _ =>
      log.info("Unsupported Message")
  }
}

trait CurrencyConverterHelper {
  def currencyRateUri: Uri
  implicit val system: ActorSystem
  implicit def executor: ExecutionContextExecutor
  implicit val materializer: Materializer

  private val CubeT = "Cube"

  def fetch: Future[Map[String, Double]] = {
    Http().singleRequest(RequestBuilding.Get(currencyRateUri)) flatMap {
      case HttpResponse(StatusCodes.OK, _, entity, _) =>
        Unmarshal(entity).to[NodeSeq]
      case HttpResponse(code, _, _, _) =>
        Future.failed(new IllegalStateException("Request failed, response code: " + code))
    } map(_ \ CubeT \ CubeT ) map toCurrencyMap
  }

  private def toCurrencyMap(cubeArray: NodeSeq): Map[String, Double] =
    (for {
      eachCube <- cubeArray \ CubeT
      currency <- eachCube \ "@currency"
      rate <- eachCube \ "@rate"
    } yield currency.toString -> rate.toString.toDouble).toMap + ("EUR" -> 1.0)

  def covert(quantity: Double,
             base: Currency,
             target: Currency,
             currencyRateState: Map[String, Double]): Either[String, Model.Money] =

    if (currencyRateState.isEmpty) {
      Left("Empty currency rate table, try again later.")
    } else if (!currencyRateState.isDefinedAt(base.getCurrencyCode) ||
               !currencyRateState.isDefinedAt(target.getCurrencyCode)) {
      Left("Unknown currency code")
    } else {
      Right(Model.Money(
        quantity / (currencyRateState(base.getCurrencyCode) / currencyRateState(target.getCurrencyCode)),
        target
      ))
    }
}
