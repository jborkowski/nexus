package com.jobo.actors

import java.util.Currency

import akka.actor.{Actor, ActorLogging, ActorRef, ActorSystem, Props}
import akka.http.scaladsl.marshalling.ToResponseMarshallable
import akka.stream.{ActorMaterializer, Materializer}
import com.jobo.actors.ActorProtocol._
import com.jobo.protocol.{Model, Protocol}
import akka.pattern.ask
import akka.pattern.pipe
import akka.util.Timeout
import cats.implicits._
import cats.data.EitherT
import com.jobo.db.Schema.DbModelExtras._

import scala.concurrent.ExecutionContextExecutor
import scala.concurrent.duration._

object ClientWallet {
  def props(contractorId: Model.ContractorId,
            cardNumber: Model.CardNumber,
            storage: ActorRef,
            currencyConvert: ActorRef) =
    Props(new ClientWallet(contractorId, cardNumber, storage, currencyConvert))
}

class ClientWallet(
  val contractorId: Model.ContractorId,
  val cardNumber: Model.CardNumber,
  storage: ActorRef,
  currencyConvert: ActorRef) extends Actor with ActorLogging with ClientWalletHelper {

  implicit val system: ActorSystem = context.system
  implicit def executor: ExecutionContextExecutor = system.dispatcher
  implicit val materializer: Materializer = ActorMaterializer()

  implicit val askTimeout: Timeout = 3 seconds

  def receive: Receive = state()

  def state(wallet: Model.Money = Model.Money(0.0D)): Receive = {
    case m: Msg => m match {
      case Transaction(products) =>
        log.info(s"Products list: $products")
        val grouped = calcProfits(products)

        (storage ? GetActiveAccountingPeriodsBy(contractorId, grouped.keySet.toSeq))
          .mapTo[AccountingPeriodsResponse]
          .map(_.accountingPeriods)
          .map(e => e.map(_.groupBy(_.categoryId).map(_._2.head))) // remove duplicates of the same categoryId
          .map(_.map { seq =>
            val m = seq map (t => (t.categoryId.toModel, t))
            val z = m.map { case (k,v) =>
              val sum = grouped.getOrElse(k, 0.0D)
              (sum / v.priceThreshold.value).toInt * v.clientSalary.value
            }
            Model.Money(z.sum)
          })
          .map(NewWalletState) pipeTo self
      case NewWalletState(w) =>
        w match {
          case Right(m) =>
            context.become(state(Model.Money(m.money + wallet.money)))
          case Left(e) =>
            log.error(s"New wallet state wasn't updated: $e")
        }

      case GetWallet(_, targetCurrency, recipient, false) =>
        import Protocol._
        import akka.http.scaladsl.model.StatusCodes._
        val DefaultCurrency = Currency.getInstance("PLN")

        (currencyConvert ? Exchange(wallet, targetCurrency.getOrElse(DefaultCurrency)))
          .mapTo[ExchangeResponse]
          .map(_.money)
          .map[ToResponseMarshallable] {
            case Right(v) => OK -> Protocol.ContractorWallet(v.money, v.currency)
            case Left(e) => BadRequest -> e
          }
          .map(CompleteRequest) pipeTo recipient

      case GetWallet(_, targetCurrency, _, true) =>
        val DefaultCurrency = Currency.getInstance("PLN")

          EitherT((currencyConvert ? Exchange(wallet, targetCurrency.getOrElse(DefaultCurrency)))
            .mapTo[ExchangeResponse]
            .map(_.money)
          )
          .map { m =>
            Model.Contractor(contractorId, Model.ContractorWallet(m.money, m.currency))
          }
          .value
          .map(GetWalletResponse) pipeTo sender()
    }
  }
}

trait ClientWalletHelper {
  def calcProfits(products: Seq[Model.Product]): Map[Model.CategoryId, Double] = {
    products
      .groupBy(_.categoryId)
      .mapValues(pSeq => pSeq.map(p => p.quantity.number * p.grossCost.money).sum)
  }
}
