package com.jobo.actors

import java.util.Currency

import akka.actor.ActorRef
import akka.http.scaladsl.marshalling.ToResponseMarshallable
import com.jobo.db.Schema
import com.jobo.protocol.Model


object ActorProtocol {
  trait Msg

  final case class Transaction(products: Seq[Model.Product]) extends Msg
  final case class NewWalletState(wallet: Either[String, Model.Money]) extends Msg

  final case class Exchange(money: Model.Money, target: Currency) extends Msg
  final case class ExchangeResponse(money: Either[String, Model.Money]) extends Msg

  final case class ConsumeTransaction(contractorId: Model.ContractorId,
                                      cardNumber: Model.CardNumber,
                                      products: Seq[Model.Product],
                                      recipient: ActorRef) extends Msg
  final case class GetWallet(cardNumber: Model.CardNumber,
                             targetCurrency: Option[Currency],
                             recipient: ActorRef,
                             group: Boolean = false) extends Msg
  final case class GetGroupWallet(groupId: Model.GroupId,
                                  targetCurrency: Option[Currency],
                                  recipient: ActorRef) extends Msg
  final case class AccountingPeriods(contractorId: Model.ContractorId,
                                     activeFilter: Option[Boolean],
                                     recipient: ActorRef) extends HandleRequest

  final case class GetWalletResponse(response: Either[String, Model.Contractor])

  trait HandleRequest extends Msg
  final case class GetWalletState(cardNumber: Model.CardNumber, targetCurrency: Option[Currency]) extends HandleRequest
  final case class ProcessTransactions(transaction: Model.Transaction) extends HandleRequest
  final case class GetAccountingPeriods(contractorId: Model.ContractorId, activeFilter: Option[Boolean]) extends HandleRequest
  final case class GetGroupWalletState(group: Model.GroupId, targetCurrency: Option[Currency]) extends HandleRequest
  final case class CompleteRequest(obj: ToResponseMarshallable) extends HandleRequest
  final case class CompleteFailureRequest(t: Throwable) extends HandleRequest

  // STORAGE
  final case class GetActiveAccountingPeriodsBy(contractorId: Model.ContractorId, categoryIds: Seq[Model.CategoryId]) extends Msg
  final case class GetAccountingPeriodsBy(contractorId: Model.ContractorId, activeFilter: Option[Boolean]) extends Msg
  final case class AccountingPeriodsResponse(accountingPeriods: Either[String, Seq[Schema.AccountingPeriod]]) extends Msg
  final case class GetContractorsBy(groupId: Model.GroupId) extends Msg
  final case class ContractorsResponse(contractorGroup: Either[String, Model.ContractorGroup]) extends Msg

  // DESCRIPTION PROVIDER
  final case class GetCategoriesByIds(ids: Seq[Model.CategoryId]) extends Msg
  final case class DescriptionsResponse(categories: Either[String, Seq[Model.Category]]) extends Msg

}
