package com.jobo.actors

import akka.actor.{Actor, ActorLogging, ActorSystem}
import akka.stream.{ActorMaterializer, Materializer}
import com.jobo.actors.ActorProtocol._
import com.jobo.db.{AccountingPeriodRepository, ContractorGroupRepository}
import com.jobo.utils.DatabaseSupport
import akka.pattern.pipe
import cats._
import cats.data._
import cats.implicits._

import scala.concurrent.ExecutionContextExecutor

class Storage extends Actor
  with ActorLogging with AccountingPeriodRepository with ContractorGroupRepository with DatabaseSupport {

  implicit val system: ActorSystem = context.system
  implicit def executor: ExecutionContextExecutor = system.dispatcher
  implicit val materializer: Materializer = ActorMaterializer()

  import com.jobo.protocol.ModelExtras._
  import com.jobo.db.Schema.DbModelExtras._

  def receive: Receive = {
    case m: Msg =>
      m match {
        case GetAccountingPeriodsBy(contractorId, activeFilter) =>
          getAccountingPeriodBy(contractorId.toDbModel, activeFilter).value.map(AccountingPeriodsResponse) pipeTo sender()
        case GetActiveAccountingPeriodsBy(contractorId, categoryIds) =>
          val seq = categoryIds.map(_.toDbModel).toList
          val seqF = getActiveAccountingPeriodBy(contractorId.toDbModel, seq)
          seqF.value.map(AccountingPeriodsResponse) pipeTo sender()
        case GetContractorsBy(groupId) =>
          getContractorByGroup(groupId.toDbModel).map(_.toModel).value.map(ContractorsResponse) pipeTo sender()
      }
  }
}
