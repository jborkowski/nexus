package com.jobo.actors

import akka.actor.{Actor, ActorLogging, ActorRef, ActorSystem, Props}
import akka.http.scaladsl.marshalling.ToResponseMarshallable
import akka.pattern.pipe
import akka.pattern.ask
import akka.http.scaladsl.model.StatusCodes._
import akka.stream.{ActorMaterializer, Materializer}
import akka.util.Timeout

import scala.concurrent.ExecutionContextExecutor
import scala.concurrent.duration._
import cats.data.EitherT
import cats.implicits._
import com.jobo.actors.ActorProtocol._
import com.jobo.protocol.{Model, Protocol}


object Supervisor {
  def prop(storage: ActorRef, currencyConvert: ActorRef, categoryDescriptionProvider: ActorRef) =
    Props(new Supervisor(storage, currencyConvert, categoryDescriptionProvider))
}

class Supervisor(storage: ActorRef,
                 currencyConvert: ActorRef,
                 categoryDescriptionProvider: ActorRef) extends Actor with ActorLogging with SupervisorHelper {

  implicit val system: ActorSystem = context.system
  implicit def executor: ExecutionContextExecutor = system.dispatcher
  implicit val materializer: Materializer = ActorMaterializer()

  override def receive: Receive = state()

  implicit val askTimeout: Timeout = 15 seconds

  def state(clientsMap: Map[Model.CardNumber, ActorRef] = Map.empty): Receive = {
    case m: Msg =>
      m match {
        case ConsumeTransaction(id, c, p, r) =>
          log.info(s"New transaction for card number: $c")
          val actorRef = clientsMap.getOrElse(c, {
            val tmpRef = context.actorOf(ClientWallet.props(id, c, storage, currencyConvert))
            val newState = clientsMap + (c -> tmpRef)
            context.become(state(newState))
            tmpRef
          })
          actorRef ! Transaction(p)
          r ! CompleteRequest("Transaction performed")
        case g @ GetWallet(card, _, recipient, false) =>
          clientsMap.get(card) match {
            case Some(ref) =>
              ref ! g
            case None =>
              recipient ! CompleteRequest(BadRequest -> "Contractor with provided card number not found")
          }
        case AccountingPeriods(c, a, r) =>
          import com.jobo.protocol.Protocol._
          import com.jobo.protocol.ModelExtras._
          import com.jobo.db.Schema.DbModelExtras._

          val aD = for {
            r <- EitherT(
              (storage ? GetAccountingPeriodsBy(c, a)).mapTo[AccountingPeriodsResponse].map(_.accountingPeriods)
            )
            cIds = r.map(_.categoryId.toModel)
            l <- EitherT(
              (categoryDescriptionProvider ? GetCategoriesByIds(cIds)).mapTo[DescriptionsResponse].map(_.categories)
            )
            rd = r.map(_.toModel).map(_.toProto(getDescription(l)))
          } yield rd

          aD
            .value
            .map[ToResponseMarshallable] {
              case Right(seq) => seq
              case Left(e) => BadRequest -> e
            }
            .map(CompleteRequest) pipeTo r
        case GetGroupWallet(g, t, recipient) =>
          val groupSum = for {
            cList <- EitherT((storage ? GetContractorsBy(g)).mapTo[ContractorsResponse].map(_.contractorGroup))
            cardNumbers = cList.contractorInfo.map(_._2)
            cardList = cardNumbers.flatMap(card => clientsMap.get(card))
            wallets <- cardList.toList.traverseU {
              ref: ActorRef  =>
                EitherT((ref ? GetWallet(Model.CardNumber(""), t, self, true)).mapTo[GetWalletResponse].map(_.response))
            }
          } yield wallets

          import Protocol._
          import akka.http.scaladsl.model.StatusCodes._
          import com.jobo.protocol.ModelExtras._

          groupSum.value.map[ToResponseMarshallable] {
            case Right(ls) =>
              OK -> Protocol.ContractorGroup(
              g.toProto,
              ls.map(_.wallet).foldLeft(Option.empty[Model.ContractorWallet]) {
                case (None, c) => Some(Model.ContractorWallet(c.value, c.currency))
                case (w, c) => w.map(o => Model.ContractorWallet(c.value + o.value, c.currency))
              }.map(_.toProto),
              ls.map(_.toProto)
            )
            case Left(e) => BadRequest -> e
          }.map(CompleteRequest) pipeTo recipient

      }
  }
}

trait SupervisorHelper {
  def getDescription(dSeq: Seq[Model.Category]): Model.CategoryId => Model.Category = {
    id: Model.CategoryId =>
      println(dSeq)
      dSeq.find(_.categoryId == id).getOrElse(Model.Category(id, Model.CategoryName("Unknown Category")))
  }
}
