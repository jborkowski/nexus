package com.jobo.actors

import akka.actor.{Actor, ActorLogging, ActorSystem}
import akka.pattern.pipe
import akka.http.scaladsl.Http
import akka.http.scaladsl.client.RequestBuilding
import akka.http.scaladsl.model.Uri
import akka.http.scaladsl.model.StatusCodes._
import akka.stream.{ActorMaterializer, Materializer}
import com.jobo.actors.ActorProtocol._
import com.jobo.protocol.{Model, Protocol}
import akka.http.scaladsl.unmarshalling.Unmarshal
import cats.data.EitherT
import cats.implicits._

import scala.concurrent.{ExecutionContextExecutor, Future}

class CategoryDescriptionProvider extends Actor with ActorLogging with CategoryDescriptionProviderHelper {

  implicit val system: ActorSystem = context.system
  implicit val executor: ExecutionContextExecutor = system.dispatcher
  implicit val materializer: Materializer = ActorMaterializer()

  val ApiUrl = Uri(context.system.settings.config.getString("category.api-url"))

  override def receive: Receive = {
    case m: Msg =>
      m match {
        case GetCategoriesByIds(ids) =>
          val res: EitherT[Future, String, Seq[Model.Category]] = for {
            d <- getCategories
            f = d.filter(c => ids.contains(c.categoryId))
          } yield f

          (res.value).map(DescriptionsResponse) pipeTo sender()
      }
  }
}

trait CategoryDescriptionProviderHelper {
  import Protocol._
  import com.jobo.protocol.ProtocolExtras._

  def ApiUrl: Uri
  implicit val system: ActorSystem
  implicit def executor: ExecutionContextExecutor
  implicit val materializer: Materializer

  def getCategories: EitherT[Future, String, Seq[Model.Category]] = {
    for {
      e <- EitherT(
        Http().singleRequest(RequestBuilding.Get(ApiUrl)) flatMap { response =>
          response.status match {
            case OK => Unmarshal(response.entity).to[Seq[Protocol.Category]].map(Right(_))
            case _ => Unmarshal(response.entity).to[String].map { _ =>
              val error = s"Category Description Service request failed with code: ${response.status}"
              Left(error)
            }
          }
        }
      )
      z = e.map(_.toModel)
    } yield z
  }
}