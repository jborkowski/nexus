CREATE TABLE "contractor_group" (
  "id"              INTEGER PRIMARY KEY NOT NULL,
  "contractor_id"   VARCHAR NOT NULL,
  "card_number"     VARCHAR NOT NULL,
  "group_id"        VARCHAR NOT NULL,
  "created_at"      TIMESTAMP WITH TIME ZONE NOT NULL
);

CREATE SEQUENCE "contractor_group_id_seq";
ALTER TABLE "contractor_group" ALTER COLUMN "id" SET DEFAULT nextval('contractor_group_id_seq');