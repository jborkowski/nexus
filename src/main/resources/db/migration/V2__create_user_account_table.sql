CREATE TABLE "user_account" (
  "id"          INTEGER PRIMARY KEY NOT NULL,
  "email"       VARCHAR NOT NULL,
  "nick"        VARCHAR NOT NULL,
  "password"    VARCHAR NOT NULL,
  "created_at"  TIMESTAMP WITH TIME ZONE NOT NULL
);

CREATE SEQUENCE "user_account_id_seq";
ALTER TABLE "user_account" ALTER COLUMN "id" SET DEFAULT nextval('user_account_id_seq');
