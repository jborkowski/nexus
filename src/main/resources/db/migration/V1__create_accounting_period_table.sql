CREATE TABLE "accounting_period" (
  "id"              INTEGER PRIMARY KEY NOT NULL,
  "contractor_id"   VARCHAR NOT NULL,
  "period_start"    TIMESTAMP WITH TIME ZONE NOT NULL,
  "period_end"      TIMESTAMP WITH TIME ZONE NULL,
  "category_id"     INTEGER NOT NULL,
  "price_threshold" DOUBLE PRECISION NOT NULL,
  "client_salary"   DOUBLE PRECISION NOT NULL,
  "system_salary"   DOUBLE PRECISION NOT NULL
);

CREATE SEQUENCE "accounting_period_id_seq";
ALTER TABLE "accounting_period" ALTER COLUMN "id" SET DEFAULT nextval('accounting_period_id_seq');
