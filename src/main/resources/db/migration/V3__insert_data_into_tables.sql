INSERT INTO accounting_period (contractor_id, period_start, period_end, category_id, price_threshold, client_salary, system_salary)
VALUES ('1a8210fc-5f3c-4820-9ed8-31f59875e65c', to_timestamp('2017-01-31 15:44:59', 'YYYY-MM-DD HH24:MI:SS') , to_timestamp('2017-12-32 15:44:59', 'YYYY-MM-DD HH24:MI:SS'), 10, 75.50, 5, 3.2);

INSERT INTO accounting_period (contractor_id, period_start, period_end, category_id, price_threshold, client_salary, system_salary)
VALUES ('1a8210fc-5f3c-4820-9ed8-31f59875e65c', to_timestamp('2017-01-31 15:44:59', 'YYYY-MM-DD HH24:MI:SS') , to_timestamp('2017-02-32 15:44:59', 'YYYY-MM-DD HH24:MI:SS'), 1, 75.50, 5, 3.4);

INSERT INTO accounting_period (contractor_id, period_start, period_end, category_id, price_threshold, client_salary, system_salary)
VALUES ('1a8210fc-5f3c-4820-9ed8-31f59875e65c', to_timestamp('2017-01-31 15:44:59', 'YYYY-MM-DD HH24:MI:SS') , null , 2, 75.50, 5, 3.3);

INSERT INTO accounting_period (contractor_id, period_start, period_end, category_id, price_threshold, client_salary, system_salary)
VALUES ('425c30de-3d5a-45eb-8889-eeff0cbf873e', to_timestamp('2017-01-31 15:44:59', 'YYYY-MM-DD HH24:MI:SS') , to_timestamp('2017-12-32 15:44:59', 'YYYY-MM-DD HH24:MI:SS'), 9, 75.50, 5, 3.4);

INSERT INTO accounting_period (contractor_id, period_start, period_end, category_id, price_threshold, client_salary, system_salary)
VALUES ('425c30de-3d5a-45eb-8889-eeff0cbf873e', to_timestamp('2017-01-31 15:44:59', 'YYYY-MM-DD HH24:MI:SS') , to_timestamp('2017-12-32 15:44:59', 'YYYY-MM-DD HH24:MI:SS'), 10, 75.50, 5, 3.4);
