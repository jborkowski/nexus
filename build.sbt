organization := "com.jobo"

name := "nexus-test-app"

version := "0.1.0"

scalaVersion := "2.12.3"

resolvers += Resolver.bintrayRepo("hseeberger", "maven")

libraryDependencies ++= {
  val akkaVersion       = "2.4.16"
  val akkaHttpVersion   = "10.0.10"
  val scalaTestVersion  = "3.0.1"
  val slickVersion      = "3.2.0"
  Seq(
    "com.typesafe.akka" %% "akka-actor" % akkaVersion,
    "com.typesafe.akka" %% "akka-stream" % akkaVersion,
    "com.typesafe.akka" %% "akka-testkit" % akkaVersion,
    "com.typesafe.akka" %% "akka-http" % akkaHttpVersion,
    "com.typesafe.akka" %% "akka-http-spray-json" % akkaHttpVersion,
    "com.typesafe.akka" %% "akka-http-xml" % akkaHttpVersion,
    "com.typesafe.akka" %% "akka-http-testkit" % akkaHttpVersion % "test",
    "org.scalatest"     %% "scalatest" % scalaTestVersion % "test",
    "org.scalamock"     %% "scalamock-scalatest-support" % "3.5.0" % Test,

    "de.heikoseeberger" %% "akka-http-circe" % "1.18.0",

    "pl.iterators" %% "kebs-slick" % "1.4.3",
    "pl.iterators" %% "kebs-spray-json" % "1.4.3",
    "pl.iterators" %% "kebs-akka-http" % "1.4.3",

    "org.slf4j" % "slf4j-nop" % "1.6.4",
    "org.flywaydb" % "flyway-core" % "3.2.1",
    "org.postgresql" % "postgresql" % "9.4-1201-jdbc41",
    "com.typesafe.slick" %% "slick-hikaricp" % slickVersion,
    "com.typesafe.slick" %% "slick" % slickVersion
  )
}
        
