## QUICK START

1. Run DB in docker:
```docker run --name nexus-db -e POSTGRES_PASSWORD=test -p 5432:5432 -e POSTGRES_DB=nexus-db -d postgres```
2. Run 'MockCategories'
3. Run 'Nexus'
4. Basic Auth: 
```aidl
user: Any
pass: p4ssw0rd
```
5. Import 'Nexus.postman_collection' to Postman:
    * collection import (???)